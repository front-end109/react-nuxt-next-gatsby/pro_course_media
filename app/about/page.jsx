import React from "react";
export const metadata = {
  title: "About Pro Course Media",
  keywords:
    "web development, web design, javascript, react, node, angular, vue, html, css",
};
const AboutPage = () => {
  return (
    <div>
      {" "}
      <h1>About Pro-course Media </h1>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem,
        necessitatibus! Et enim qui iure, quisquam temporibus pariatur rem est
        perferendis doloribus eius placeat dolorem voluptatem? Non hic rerum
        dolor tempora unde natus, fugit molestias dolore a voluptatum aliquid
        illo recusandae! Dolor numquam exercitationem accusantium consequatur
        maiores aperiam earum a sit!
      </p>
    </div>
  );
};

export default AboutPage;
